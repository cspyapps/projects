# -*- coding: utf-8 -*-
"""
Created on Tue Jun 20 15:19:15 2017

@author: CS029534

matplotlib surface plotting help at link below:
https://matplotlib.org/mpl_toolkits/mplot3d/tutorial.html

matplotlib named colors reference:
https://matplotlib.org/examples/color/named_colors.html

colormaps:
https://matplotlib.org/users/colormaps.html

zorder plotting issue:
https://stackoverflow.com/questions/23188561/matplotlib-3d-plot-zorder-issue

marker types help:
Type 'markers?' in the console after loading all packages
"""

#change working directory to script location
import os
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

#load required packages
import numpy as np
import pandas as pd
from optproblems_helpers import *
import matplotlib.pyplot as plt
from matplotlib import cm
from scipy.interpolate import griddata


#USER SETTINGS
###############################################################################
#I/O settings
data_path =  r'..\data\rw_sorted.csv' #path to input data
fig_name = 'DemandVisualization3D'     #don't include ext. None 4 no save
dpi = 500                                   #fig resolution in dots per inch
views = [(30, 300), (60, 120), (0, 300)]    #list of (elev, azim) tuples

#Surface appearance params
cmap = cm.hot                   #cm.hot, cm.PuBu
surface_shade = True             #whether to shade the surface faces
surface_alpha = 0.7             #transparency, where 1 is opaque
   
#Axis settings                              
xaxis = 'Timestep' #'Timestep'
yaxis = 'Percentile' #'Percentile'
zaxis = 'Value'  #'Value'
demand_cat = 'Customer' # {'Customer', 'Terminal', 'Total'}

#label settings
xlabel = 'Timestep'          #x axis label
ylabel = 'Percentile'          #y axis label
zlabel = 'Customer Demand (MGD)'      #z axis label

###############################################################################

#read in data
df = pd.read_csv(data_path)

#subset df based on demand category
df = df[df['Category'] == demand_cat]

#generate x, y 1D arrays (length L) and extract the 1D z array (length L^2)
#from dataframe
x = df[xaxis]
y = df[yaxis]
z = df[zaxis]

#convert 1D arrays to meshgrids for surface plotting
X, Y = np.meshgrid(x, y)
Z = griddata(df[[xaxis, yaxis]], z, (X, Y), method='linear')

#initiate a fig and axes3D object
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d') #111 specifies 1x1 grid, 1st subplot

#create plot 
ax.plot_surface(X, Y, Z, shade = surface_shade, cmap = cmap, 
                alpha = surface_alpha) 

#set axes labels
ax.set_xlabel(xlabel)
ax.set_ylabel(ylabel)
ax.set_zlabel(zlabel)

#save figure for each specified view if fig_name exists
if fig_name:
    for elev, azim in views:
        ax.view_init(elev, azim)
        fig_path = r'..\figs\%s_%d_%d.png' % (fig_name, elev, azim)
        fig.savefig(fig_path, dpi = dpi)
        fig.show()

