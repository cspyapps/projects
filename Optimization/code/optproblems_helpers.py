# -*- coding: utf-8 -*-
"""
Created on Tue Jun 27 18:05:12 2017

@author: CS029534
"""

#%#############################################################################
#General optproblems helper functions
#%#############################################################################

import numpy as np
import pandas as pd
from scipy.optimize import fmin

def getSolutionDFShape(solutions_list):
    
    '''  
    Get dataframe shape where each row is a solution and each column
    is either a decision variable or objective function value.

    Returned value is a tuple made of two tuples (each of length 2). 

    First tuple = (nrows, ncols)
    Second tuple = (nvars, nobjs). Note nvars + nobjs = ncols
    '''  
    
    nrows = len(solutions_list)
    nvars = len(solutions_list[0].phenome)
    
    
    float_types = ("<type 'float'>", "<class 'float'>", "<type 'numpy.float64'>", \
                   "<class 'numpy.float64'>")
    
    sample_objval = solutions_list[0].objective_values
    
    if isinstance(sample_objval, float):
        nobjs = 1
    else: 
        nobjs = len(solutions_list[0].objective_values)
        
    ncols = nvars + nobjs
    
    return(((nrows, ncols), (nvars, nobjs)))


def solutionToDF(solutions_list):
    
    '''
    Extract the decision variable values and objective function values from 
    optproblems solutions class instance to pandas df.
    '''   
    
    #get dataframe shape
    df_shape_info = getSolutionDFShape(solutions_list)
    df_shape = df_shape_info[0]
    nvars = df_shape_info[1][0]  #second tuple first element
    nrows = df_shape[0]
    ncols = df_shape[1]
        
    #preallocate dataframe
    df = pd.DataFrame(np.zeros(df_shape))
    
    
    for k in range(nrows):
        
        df.iloc[k, 0:(nvars)] = solutions_list[k].phenome #make col indx general
        df.iloc[k, nvars:ncols] = solutions_list[k].objective_values #make col indx general
    
    
    return(df)



def arrayToMeshgrid(x, y, z):
    
    '''
    Takes 1D arrays x, y (each of length L) and 1D array z (length L^2). 
    
    z = func(x, y) where cycling through elements in x, then y to enumerate
    all possible combinations

    Returns meshgrids, 2D arrays (LxL) for plotting a 3D surface 
    (specifically using matplotlib pyplot's plot_surface function).
    '''
    
    X, Y = np.meshgrid(x, y)
    Z = z.values.reshape(X.shape)
    
    return X, Y, Z



def generateVarLevelSets(xmin, xmax, ymin, ymax, num_levels):

    '''
    Given x and y bounds and a number of value levels, generates a list of 
    length n where each element is a tuple of length 2, containing a unique
    combination of x, y variable values. 
    
    n = num_levels^2
    '''
    
    #create evenly spaced DV values between within x and y ranges
    x = np.linspace(xmin, xmax, num_levels)
    y = np.linspace(ymin, ymax, num_levels)
    
    #create 2D meshgrids of DV values
    X, Y = np.meshgrid(x, y)
    
    #create list of tuples with all x, y DV value combinations
    a = zip(np.ravel(X), np.ravel(Y))
    
    return(a)

def find_minimum(obj_function, x0):
    
    '''
    Given a 2-variable objective function obj_function [function] and an inital 
    guess array of length 2 [numpy.ndarray], returns a tuple of length 3
    containing the the optimization algorithm's guesses (x1 and x2) and the 
    resulting solution value (f(x1, x2)) for all iterations.  The length of
    each of the three lists will vary depending on the number of iterations
    executed by the optimization algorithm prior to converging on a solution.
    
    Returns: 
    A tuple of length 3, where each element is a list of floats.
    First list contains the x1 guesses
    Second list contains the x2 guesses
    Third list contains the f(x1, x2) solutions
    '''
    
    #run the optimization
    res = fmin(func = obj_function, x0= x0, retall = True)
    
    #get the guesses
    guesses = res[1]
    x_guess = [float(guesses[i][0]) for i in range(len(guesses))]
    y_guess = [float(guesses[j][1]) for j in range(len(guesses))]
    
    #map the function to the guesses to get solutions array
    z_guess = map(obj_function, guesses)
    z_guess = map(float, z_guess)
    
    return((x_guess, y_guess, z_guess))
    
    
    
    