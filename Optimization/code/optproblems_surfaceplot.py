# -*- coding: utf-8 -*-
"""
Created on Tue Jun 20 15:19:15 2017

@author: CS029534

DTLZ test problem setup based on the Parallelism Example from link below:
https://ls11-www.cs.tu-dortmund.de/people/swessing/optproblems/doc/base.html#

matplotlib surface plotting help at link below:
https://matplotlib.org/mpl_toolkits/mplot3d/tutorial.html

matplotlib named colors reference:
https://matplotlib.org/examples/color/named_colors.html

colormaps:
https://matplotlib.org/users/colormaps.html

zorder plotting issue:
https://stackoverflow.com/questions/23188561/matplotlib-3d-plot-zorder-issue

marker types help:
Type 'markers?' in the console after loading all packages
"""

#change working directory to script location
import os
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

#load required packages
import diversipy
from optproblems import dtlz
from optproblems import continuous
from optproblems import * 
import numpy as np
import pandas as pd
import math
import random
from optproblems_helpers import *
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib.markers as markers
from matplotlib import cm


#user settings
###############################################################################
num_objectives = 1  #set to 1 for surface plotting (z axis)
num_vars = 2      #set to 2 for surface plotting (x, y axes)
num_levels= 50      #(num_levels)^2  = num_solutions

xmin = -1.9           #lower bound of x variable
xmax = 1.9          #upper bound of x variable
ymin = -1            #lower bound of y variable
ymax = 1            #upper bound of y variable

x0 = [-1.2, -0.75]    #initial guess for opt soln, list or tuple of length 2
#good trap guess = [-1.5, -0.9], good successful guess = [-1.2, -0.75]

cmap = cm.hot #cm.hot, cm.PuBu
surface_shade = True             #whether to shade the surface faces
surface_alpha = 0.7

marker_color = 'b'       #color of the point markers darkorange
marker_size = 12                 #relative size of the point markers
marker_type = 'o'                #type/shape of the points; help above
init_guess_marker_type = 'x'    #can specify diff marker type for init guess
marker_alpha = 1              #marker transparency, where 1 is opaqued
depthshade = True           #shade to give appearance of depth

xlabel = 'x'          #x axis label
ylabel = 'y'          #y axis label
zlabel = 'f(x, y)'      #z axis label

fig_name = 'SixHumpCamelBack_GoodGuess'   #don't include ext. None for no save
dpi = 500                           #output figure resolution in dots per inch
views = [(30, 300), (60, 120), (0, 300)] #list of (elev, azim) tuples

optimizer_alg = find_minimum  #algorithm function for finding the min
                                 #must support f(objfun, guess) arguments
###############################################################################


#create an instance of the DTLZ2 opt test problem class
#func = dtlz.DTLZ2(num_objectives, num_vars)
'''
The DTLZ and continuous test problems (different modules within the optproblems
package) are formulated slightly differently. For example:
    
    DTLZ:   optproblems.dtlz.DTLZ2 is the class and optproblems.dtlz.DTLZ2.g
    is the function
    continuous: optproblems.continuous.SixHumpCamelback is the class and 
    optproblems.continuous.six_hump_camelback is the function. But this function
    is also accessible via a.objective_function when a is instantiated as 
    follows: a = optproblems.continuous.SixHumpCamelback() [alias?]
    
'''
func = continuous.SixHumpCamelback()

#set up test problem; g is the obj function (method) of the DTLZ2 class
#problem = TestProblem(func.g, num_objectives = num_objectives)
problem = TestProblem(func.objective_function)

#generate x, y variable level combinations list
a = generateVarLevelSets(xmin, xmax, ymin, ymax, num_levels)

#list comprehension to generate list of solutions
solutions = [Individual(i) for i in a]

#evaluate the solutions
problem.batch_evaluate(solutions)

#convert to pandas dataframe
solution_df = solutionToDF(solutions)

#generate x, y 1D arrays (length L) and extract the 1D z array (length L^2)
#from dataframe
x = np.linspace(xmin, xmax, num_levels)
y = np.linspace(ymin, ymax, num_levels)
z = solution_df[2]

#convert 1D arrays to meshgrids for surface plotting
X, Y, Z = arrayToMeshgrid(x, y, z)

#initiate a fig and axes3D object
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d') #111 specifies 1x1 grid, 1st subplot

#set useful surface plot settings, then create plot
#ax.plot_surface(X, Y, Z, color = surface_color, shade = surface_shade), 
ax.plot_surface(X, Y, Z, shade = surface_shade, cmap = cmap, 
                alpha = surface_alpha) 

#get "guesses" to plot as points
#for the example, plot all df points (all will be on surface, and at vertex)
gx = solution_df[0]
gy = solution_df[1]
gz = solution_df[2]

#use optimization algorithm to find the minimum
guesses = optimizer_alg(func.objective_function, x0)

#useful scatterplot settings
ax.scatter(xs = guesses[0][0], ys =guesses[1][0], zs = guesses[2][0], 
           c = marker_color, s = marker_size*2, depthshade = depthshade, 
           alpha = marker_alpha, marker = init_guess_marker_type)
ax.scatter(xs = guesses[0][1:], ys =guesses[1][1:], zs = guesses[2][1:], 
           c = marker_color, s = marker_size, depthshade = depthshade, 
           alpha = marker_alpha, marker = marker_type)

#set axes labels
ax.set_xlabel(xlabel)
ax.set_ylabel(ylabel)
ax.set_zlabel(zlabel)

#save figure if fig_name exists
if fig_name:
    for elev, azim in views:
        ax.view_init(elev, azim)
        fig_path = r'..\figs\%s_%d_%d.png' % (fig_name, elev, azim)
        fig.savefig(fig_path, dpi = dpi)
        fig.show()



