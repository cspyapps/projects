# -*- coding: utf-8 -*-
"""
Created on Mon Apr 22 14:47:38 2019

@author: CS029534
"""


testing = False

import os
import pandas as pd
import h5py
import eventdefsum as eds
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor

#Filepaths
data_dir = r"\\hchfpp01\Groups\WBG\MillCreek\Analysis\Prediction\data"
h5_filename = "MuddyCreekData.h5"
working_dir = r"\\hchfpp01\Groups\WBG\MillCreek\Analysis\Prediction\working" #where to write files
out_filename = r"param_summary_MUDDY.csv"

#settings
field = "rainfall"  #variable used to define events
IE_hr = 12          #inter-event period, in hours
ts = 5              #timestep interval of input data, in minutes
thresh = 0.001      #minimum value of 'field' to consider non-zero
resp_var = 'overflow_sum'    #response variable for stat modeling



nIterations = 10 # number of models to create, using different train-test splits
keep_vars = ['rainfall_sum', 'rainfall_max', 'cso_level_mean',
             'duration_hrs', 'overflow_sum']


# open h5 file and get list of all groups
h5_fp = os.path.join(data_dir, h5_filename)
h5 = h5py.File(h5_fp)
list_groups = [x for x in h5.keys()]
h5.close()

# check testing
if(testing):
    list_groups = list_groups[0:2]

df_master_headers = ['CSO', 'Iteration', 'VarLabel', 'Importance']
list_master = []

nPredVars = len(keep_vars) - 1

for cso in list_groups:
    
    print('Working on {}...'.format(cso))
    
    try:
        # read in data, filling missing vals, summarize events
        df = pd.read_hdf(h5_fp, cso)
        df.fillna(method = "bfill", inplace = True)
        df_event = eds.define_events(df, field, IE_hr, ts, thresh)
        df_event = eds.addDateTimeColumnFromIndex(df_event)
        aggmap = eds.get_MSD_aggmap()
        df_event_summary = eds.summarize_events(df_event, aggmap)
        
        # subset df fields
        df_model = df_event_summary[keep_vars]
        
        X = df_model.iloc[:,:-1]
        y = df_model.iloc[:,-1]
    
        for i in range(nIterations):
            print('    iteration {}'.format(i+1))
            
            X_train, X_test, y_train, y_test = train_test_split(X, y, test_size= 0.3, random_state = i)
            
            rf_model = RandomForestRegressor(n_estimators= 1000, max_features= 0.33, 
                                      n_jobs= -1, random_state = i)
            rf_model.fit(X_train, y_train)
            
            feat_imp = rf_model.feature_importances_
            
            for fi in range(nPredVars):
                new_row = []
                new_row.append(cso)
                new_row.append(i)
                new_row.append(keep_vars[fi])
                new_row.append(feat_imp[fi])
                list_master.append(new_row) #add to master
    except:
        pass
        
# write output
df_out = pd.DataFrame(list_master)
df_out.columns = df_master_headers
path_out = os.path.join(working_dir, out_filename)
df_out.to_csv(path_out, index = False)
