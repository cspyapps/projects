# -*- coding: utf-8 -*-
"""
Created on Fri Dec 28 15:40:01 2018

@author: CS029534
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.metrics import r2_score
from statsmodels.stats.outliers_influence import variance_inflation_factor

def save_pairplot(df, fp_out, trans = False):
    pp = sns.pairplot(df)
    pp.savefig(fp_out, dpi = 500, trans = trans)
	

def build_sm_formula(pred_vars, resp_var):
    return resp_var + " ~ " + (' + '.join(pred_vars))

def get_vifs(df, exclude = None):
    """
    Given a dataframe with the response variable in the last column and
    predictor variables in all other columns, calculates the Variance
    Inflation Factor (VIF) for each predictor variable. 
    VIF values above 5 warrant review of the model for multicollinearity.
    
    See https://en.wikipedia.org/wiki/Multicollinearity
    
    Note that the dataframe index is not used.
    """
    if exclude is not None:
        df = df.drop(labels = exclude, axis = 1).copy()
    matrix = df.iloc[:,:-1].values
    label_list = []
    value_list = []
    for j in range(matrix.shape[1]):
        label_list.append(list(df.columns)[j])
        value_list.append(variance_inflation_factor(matrix, j))
    return pd.Series(data = value_list, index = label_list)


def plot_actual_vs_pred(y_pred, y_act):
    plt.scatter(y_pred, y_act)
    plt.xlabel("Predicted", fontsize = 14)
    plt.ylabel("Actual", fontsize = 14)
    axlim = max(y_pred.max(), y_act.max())
    plt.plot(np.linspace(0,axlim,1000),np.linspace(0,axlim,1000), 
             color = 'r', alpha = 0.3)
    plt.text(0, axlim*0.95, "$R^2$={}".format(round(r2_score(y_act, y_pred), 3)))
    plt.show()
