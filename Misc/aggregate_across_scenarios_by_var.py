# -*- coding: utf-8 -*-
"""
Created on Wed Dec 20 09:00:52 2017

@author: CS029534
"""

#aggregate by variable, across all files (scenarios)
#filenames used as column headers

import os
import pandas as pd

input_dir = r"C:\Users\cs029534\Documents\BitBucket_Projects\data\MSP\input"
output_dir = r"C:\Users\cs029534\Documents\BitBucket_Projects\data\MSP\output\csv"
target_vars = ["UnmetDemand", "BreakLowPressureDemand", "IsolLowPressureDemand"]


files = [input_dir + "\\" + f for f in os.listdir(input_dir) if f.endswith("csv")]

#get a set of all pipe IDs
pipe_set = set()
for f in files:
    tdf = pd.read_csv(f)
    keep_MainTypes = set(tdf["MainType"]) - set(["BlowOff", "HydrantLaterals"])
    print('Dataframe shape before dropping MainTypes: {}'.format(tdf.shape))
    tdf = tdf[tdf['MainType'].isin(keep_MainTypes)]
    print('Dataframe shape after dropping MainType: {}'.format(tdf.shape))
    pipe_set.update(tuple(tdf.loc[:,"ID"].tolist()))


for var in target_vars:
    df = pd.DataFrame(index = pipe_set) #instantiate a df with full pipe id index
    for f in files:
        colname = os.path.split(f)[1].split(".")[0]
        series = pd.read_csv(f, index_col="ID").loc[:,var]
        df[colname] = series
    df.to_csv(output_dir + "\\" + var + "_ValuesAcrossScenarios.csv")
