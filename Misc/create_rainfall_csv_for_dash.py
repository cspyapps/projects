# -*- coding: utf-8 -*-
"""
Created on Fri Jan 26 12:28:30 2018

@author: CS029534
"""



#User settings-----------------------------------------------------------------
cso_rainfall_config_fp = r'.\data\cso_rainfall_config.csv'
cso_tag_columns = (0, 2) #tuple of column indices for cso name and tag in config
ext_data_config_fp = r'.\data\get_rainfall_data.xml' #simlink ext data format xml
out_rainfall_fp = r'.\data\rainfall_data_long.csv'
start_datetime = (2018, 2, 3, 0, 0, 0) #(yyyy, [m]m, [d]d, [h]h, [m]m ,[s]s)
end_datetime =(2018, 2, 12, 23, 55, 0) #(yyyy, [m]m, [d]d, [h]h, [m]m ,[s]s)
time_interval = 5 #minutes
temp_file_fp = r'.\data\temp.csv'
write_format = "long" # one of "crosstab" or "long"
#------------------------------------------------------------------------------


#import modules
import numpy as np
import pandas as pd
import math
import os
import re
import subprocess
import xml.etree.ElementTree as ET

#define simlink xml helper function
def set_xml_values(xml_fp, element_indx_list, values_list):
    
    tree = ET.parse(xml_fp)
    root = tree.getroot()
    num_updates = len(values_list)
    
    for i in range(num_updates):
        root[0][element_indx_list[i]].set('Value', values_list[i])
    
    tree_out = ET.ElementTree(root)
    tree_out.write(xml_fp)

#regex match helper
def is_regex_match(regex, string):  
    return True if (re.match(regex, string) <> None) else False
 
#log file cleanup helper function
def rakeburn_files(directory, file_regex):  
    files = os.listdir(directory)
    raked_files = [f for f in files if is_regex_match(file_regex, f)]
    for k in raked_files:
        os.remove(k)
    

#convert datetime component tuples into pandas datetime objects
start_dt = pd.datetime(*start_datetime) #* unpacks the tuple
end_dt = pd.datetime(*end_datetime)

#enforce absolute filepaths for externally referenced filepaths
ext_data_config_fp = os.path.abspath(ext_data_config_fp)
temp_file_fp = os.path.abspath(temp_file_fp)

#generate a tag -> cso dictionary from cso config file data
cso_rainfall_config_df = pd.read_csv(cso_rainfall_config_fp)
cso_list = pd.Series(cso_rainfall_config_df.iloc[:,cso_tag_columns[0]]).tolist()
tag_list = pd.Series(cso_rainfall_config_df.iloc[:,cso_tag_columns[1]]).tolist()
tag_cso_dict = dict(zip(tag_list, cso_list))

#group tags into bins of 10 for the request 
tag_list_request = []
tag_group = ""
k = 0 #group counter
mk = 0 #master counter- never reset
kstop = len(tag_list)
for tag in tag_list:
    if len(tag_group)== 0:
        tag_group += tag
        k += 1
        mk += 1
    else:
        tag_group += ","+tag
        k += 1
        mk += 1
    if k== 10 or mk == kstop: #push tag_group to list and reset
        tag_list_request.append(tag_group)
        tag_group = ""
        k = 0
    

#determine number of days in the time period and round up
duration = int(math.ceil(((end_dt-start_dt).total_seconds())/86400))
dayrange = range(duration)

#make xml updates that are constant across all data requests (temp filename, samp int)
set_xml_values(ext_data_config_fp, [3,5], [str(time_interval), temp_file_fp])

#create a master df to hold all data
timestamp = map(str, pd.date_range(start_dt, end_dt, freq = str(time_interval)+"min"))
df = pd.DataFrame(index = timestamp, columns = tag_list)

#calc number of loops for tracking progress
num_loops = len(tag_list_request)*len(dayrange)
loop = 0
tgc = 0 #tag group counter

#loop through each tag
for tag in tag_list_request: #using groupings
    
    tgc += 1
    
    #set tag the xml doc
    set_xml_values(ext_data_config_fp, [4], [tag])
    
    #initialize the datetimes
    t0_init = start_dt
    t1_init = (start_dt + pd.DateOffset(minutes = (1440 - time_interval))).to_pydatetime()
    
    
    for day in dayrange:
        
        #create the current start/end datetime strings
        t0 = (t0_init + pd.DateOffset(days = day)).to_datetime() #keep moving by 1 day
        t1 = (t1_init + pd.DateOffset(days = day)).to_datetime()
        t0_str = str(t0)
        t1_str = str(t1)
        
        #status updates
        loop += 1
        print("Working on data request {a}/{b}: Tag Group {c} on {d}".format(a = loop, 
              b = num_loops, c = tgc, d = t0_str[0:11]))
        
        #update xml file
        set_xml_values(ext_data_config_fp, [1,2], [t0_str, t1_str])
        
        #write a .bat file with simlink command
        command = "simlink -external_data -config " + ext_data_config_fp
        bat_file = open("get_data_with_simlink.bat", 'w')
        bat_file.write(command)
        bat_file.close()
        
       
        
        #call simlink- wait for the Press Any Key command and send ENTER to continue        
        p = subprocess.Popen("get_data_with_simlink.bat",
                     bufsize=1, stdin= subprocess.PIPE, stdout=subprocess.PIPE)
                     #creationflags = CREATE_NO_WINDOW)
        
        while p.poll() is None:
            line = p.stdout.readline()
            if line.startswith('Press any key to continue'):
                p.communicate('\r\n')
                
        #rake up .log files and destroy
        rakeburn_files(os.getcwd(), ".*log$")
        
        #read the temp csv data and fill into master df
        tempdf = pd.read_csv(temp_file_fp, index_col= 'timestamp')
        df.update(tempdf)
        

#rename the columns to reflect CSO names instead of tag names
df.rename(columns = tag_cso_dict, inplace = True)
df["timestep"] = df.index.tolist() #pull from index to data column

if write_format == "crosstab":
    cols = df.columns.tolist()
    num_cols = len(cols)
    new_cols = ["timestep"]
    new_cols.extend(cols[0:(num_cols-1)])
    df = df[new_cols]
    df.to_csv(out_rainfall_fp, index = False)

elif write_format == "long":

    #change from column-based format to "long" format (one value per row, where all
    #preceding columns decsribe value metadata essentially)
    #This can be done easily in pandas with the melt method
    df_out = df.melt(id_vars = "timestep", var_name = "Label", value_name = "rainfall") 
    df_out = df_out[["Label", "timestep", "rainfall"]] #reorder
    
    #write data to the user-specific output location
    df_out.to_csv(out_rainfall_fp, index= False)



        


