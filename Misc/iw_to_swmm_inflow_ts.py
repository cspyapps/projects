# -*- coding: utf-8 -*-
"""
Created on Wed Oct 04 08:55:22 2017

@author: CS029534
"""

dir_in = r'F:\proj\Analysis\OptimaticsOptimization\ModelConversion\Area3ABC\qnode_csvs'
fp_out = r'F:\proj\Analysis\OptimaticsOptimization\ModelConversion\Area3ABC\Inflows\Area3ABC_InflowTS.txt'


import re
import os
import pandas as pd


start_datetime = pd.to_datetime('2001-01-01 00:00:00')
freq = '5min'
periods = 61 #could get from shape of df

"""
Inputs: qnode csvs directory, output inp file location
Outputs: writes the TIMESERIES table to a .txt file 
(to be copied & pasted the text into the target inp file)

- Converts node inflow timeseries in all csvs within the specified directory 
 into a SWMM5-format TIMESERIES table (written to a separate .txt file).
 Create csvs via InfoWorks results export of qnode for all drainage nodes, do
 not include units (single header row only) and select user units
- The name of the timeseries follows the nomenclature nodeid_suffix where 
 suffix is any text following a double underscore int the csv filename 
 (excluding the extension).  For example, for a node_id = 3A1234 within a 
 csv file named Area3__WWF_025.csv, the timeseries object name would be 
 3A1234_WWF_025 
- The date will be set to 1/1/2001 (currently cannot change, which assumes 
<24 hour event)
- The time will be taken from the qnode csvs
- The value will be taken directly from the qnode csvs
"""


csvs = os.listdir(dir_in)
csv_fps = [dir_in + '\\' + i for i in csvs]

suffixes = ['_' + re.findall('.*__(.*)\.\\w+', i)[0] for i in csvs]

#set up date and time series
ts = pd.date_range(start_datetime, periods = periods, freq = freq)
times = map(str, ts.time.astype(str))
dates = map(str, pd.Series(ts).dt.strftime('%m/%d/%Y'))

obj = []
dts = []
tms= []
val = []

for fi, f in enumerate(csv_fps):
    
    df = pd.read_csv(f)
    df = df.drop(['Time', 'Seconds'], axis = 1)
    nodes = list(df.columns)
    
    print('working on file {}'.format(suffixes[fi]))
    
    for ni, n in enumerate(nodes):
        
        for i in range(periods):
            
            obj.append(n + suffixes[fi])
            dts.append(dates[i])
            tms.append(times[i])
            val.append(df.iloc[i, ni])

print('writing file to {}'.format(fp_out))          
df_out = pd.DataFrame(zip(obj, dts, tms, val))
df_out.to_csv(fp_out, sep = ' ', index = False, header = False)
print('wrote {} lines'.format(len(val)))       
