# -*- coding: utf-8 -*-
"""
Created on Wed Nov 29 20:00:01 2017

@author: Craig Snortheim
"""

#MIT ECE 6046: Introduction to Algorithms
#Problem Set 1, Question 1-3: Unimodal Search
#give an algorithm to find the unimodal maximum in O(lgn) time

import numpy as np
import random
import matplotlib.pyplot as plt

istesting = False

def generate_unimodal_array(length):
    """
    Generates a unimodal array of with number of elements = length. Monotonically
    increases up to a global maximum at which point the array monotonically
    decreases.
    """
    m = random.randrange(1, length - 1)
    s1 = [random.random() for _ in range(m)]
    s1.sort()
    M = s1[m-1]
    s2 = [random.random() for _ in range(length-m)]
    s2.sort(reverse = True)
    return s1 + s2

if istesting: 
    for _ in range(100):
        plt.plot(generate_unimodal_array(100))
        n_test = [5, 10, 900]
    for n in n_test:
        assert(n == len(generate_unimodal_array(n)))
    
    
def find_max_index(array):
    
    
    n = len(array)
    i = round(n/2)
    
    x0 = 0 #lower bound
    x1 = n-1 #upper bound
    
    cval = array[i]
    val_prev = array[i-1] if i>0 else array[i]
    val_next = array[i+1] if i<(len(array)-1) else array[i]
    
    print(i, cval, val_prev, val_next)
    
    if (val_prev < cval and val_next < cval):
        return((i, cval))
    
    while True:
        
        print(i, cval, val_prev, val_next)
        
        if val_prev < cval and val_next < cval:
            return((i, cval))
        elif val_prev < cval and val_next > cval: #increasing
            x0 = i
            i = round((i + (x1-1))/2) + round(random.random()) #jitter to avoid getting stuck
        elif val_prev > cval and val_next < cval: #decreasing
            x1 = i
            i = round((i-x0)/2) + round(random.random()) #jitter to avoid getting stuck
        elif val_prev == cval and val_next < cval:
            return((i, cval)) #max is at start of array
        elif val_next == cval and val_prev < cval:
            return((i, cval)) #max is at end of array
        else:
            raise Exception("Array should be monotonically increasing then decreasing.")
            
        cval = array[i]
        val_prev = array[i-1] if i>0 else array[i]
        val_next = array[i+1] if i<(len(array)-1) else array[i]
        
        
array = generate_unimodal_array(100)
plt.plot(array)
maxres = find_max_index(array)
print(maxres)
        

            
    
    