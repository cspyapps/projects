# -*- coding: utf-8 -*-
"""
Created on Sat Feb 17 15:51:18 2018

@author: CS029534
"""


import numpy as np
import pandas as pd
import h5py
import os
import re
from datetime import datetime
from datetime import timedelta
import xml.etree.ElementTree as ET
from sklearn.preprocessing import StandardScaler
from time import localtime



#xnl editing function
def set_xml_values(xml_fp, element_indx_list, values_list):
    
    tree = ET.parse(xml_fp)
    root = tree.getroot()
    num_updates = len(values_list)
    
    for i in range(num_updates):
        root[0][element_indx_list[i]].set('Value', values_list[i])
    
    tree_out = ET.ElementTree(root)
    tree_out.write(xml_fp)


#file cleanup function
def rakeburn_files(directory, file_regex):  
    files = os.listdir(directory)
    raked_files = [f for f in files if is_regex_match(file_regex, f)]
    for k in raked_files:
        os.remove(k)
        
def get_pretty_date(dt):
    #returns a string in the format yyyymmdd for appending to filenames
    #given a tuple of ints (yyyy, [m]m, [d]d, [h]h, [m]m ,[s]s)
    #Example: input (2017, 3, 29, 23, 10, 17) would return "20180329"
    #supports tuple and datetime object inputs
    
    if isinstance(dt, tuple):
        y = str(dt[0])
        m = str(dt[1])
        d = str(dt[2])
        m = "0"+m if len(m) == 1 else m
        d = "0"+d if len(d) == 1 else d
        return y+m+d
    elif isinstance(dt, datetime):
        y = str(dt.year)
        m = str(dt.month)
        d = str(dt.day)
        m = "0"+m if len(m) == 1 else m
        d = "0"+d if len(d) == 1 else d
        return y+m+d
        


def get_start_date(scen_dir, offset_min = 0):
    
    dat_files = [f for f in os.listdir(scen_dir) if is_regex_match(".*dat$", f)]
    
    if len(dat_files) == 0:
        raise Exception("No .dat files found!")
    
    some_dat_fp = os.path.join(scen_dir, dat_files[0]) #take the first
    
    #assumes the following dat file schema: mm/dd/yyyy hh:mm val
    dt_list = pd.read_table(some_dat_fp, header = None, nrows= 1).iloc[0, 0:2].tolist()
    month, day, year = map(int, dt_list[0].split("/"))
    hour, minute = map(int, dt_list[1].split(":"))
    dt = datetime(year, month, day, hour, minute) + timedelta(minutes = offset_min)
    return dt
    
def get_run_date(scen_dir):
    lct = localtime(os.path.getctime(scen_dir))
    dt = datetime(lct.tm_year, lct.tm_mon, lct.tm_mday, lct.tm_hour, lct.tm_min)
    return dt

def get_h5_fp(eg_dir, scenario = None):
    """
    Returns the full filepath to the h5 file within the evaluation
    group folder and scenario specified. 
    If scenario == None (default), then the most recent scenario is used, based
    on the highest scenario number. 
    
    """
    
    if scenario != None:
        
        try:
            scenario = str(int(scenario))
        except: 
            raise Exception("Invalid scenario ID passed. Should be whole number entered as str or int.")
        
        directory = os.path.join(eg_dir, scenario)
        
        if not (os.path.exists(directory)):
            raise Exception("The specified evaluation group/scenario directory does not exist.")
        
    else: 
        
        if not (os.path.exists(eg_dir)):
            raise Exception("The specified evaluation group directory does not exist.")
        
        scens = [int(s) for s in os.listdir(eg_dir) if is_scenario_dirname(s)]
        last_scen = str(max(scens))
        directory = os.path.join(eg_dir, last_scen)
        
        
    files = os.listdir(directory)
    h5_fn = [f for f in files if is_regex_match(".*h5$", f)]
    
    num_h5 = len(h5_fn)
    if num_h5 > 1:
        raise Exception("More than one h5 file found. Check the specified directory.")
    elif num_h5 == 0:
        raise Exception("No h5 file found. Check the specified directory.")
        
    h5_fp = os.path.join(directory, h5_fn[0])
    return h5_fp
        
            

#scenario ID directory check helper
def is_scenario_dirname(name):
    try:
        x = int(name)
        return True
    except:
        return False

#regex match helper
def is_regex_match(regex, string):  
    return True if (re.match(regex, string) <> None) else False


def read_h5_file(h5_fp, df_config_eg, lst_ts_labels, cols_out, start_date, ts_min):
     
    h5 = h5py.File(h5_fp, "r")
    df_out = pd.DataFrame(columns = cols_out)
    for row in df_config_eg.iterrows():
        df_tmp = get_h5_data(h5, row, lst_ts_labels, cols_out, start_date, ts_min)  # get a list of individual string object
        df_out = df_out.append(df_tmp, ignore_index = True)
    return df_out.reset_index(drop = True)



def get_h5_data(h5, row, lst_ts_labels, cols_out, start_date, ts_min):
    #retrieve the h5 datasets for the requested period:::
    #row var is from iterrows method. Returns (index, named Series) tuples so row[1] gets the named series, then a value is retrieved by name (from records list).
    #within each h5 GROUP (TS_xxxxx) there is just one DATASET named "1" (Simlink default write method)- so the ['1'] grabs this dataset from group
    #finally, the np.array() converts from an h5py dataset type to np.array and ravel flattens into a 1d array (although should already be a 1d array)
    
    y1 = np.array(h5["TS_" + str(int(row[1][lst_ts_labels[0]]))]['1']).ravel()
    y2 = np.array(h5["TS_" + str(int(row[1][lst_ts_labels[1]]))]['1']).ravel()
    y3 = np.array(h5["TS_" + str(int(row[1][lst_ts_labels[2]]))]['1']).ravel()
    y4 = np.array(h5["TS_" + str(int(row[1][lst_ts_labels[3]]))]['1']).ravel()
    y5 = np.array(h5["TS_" + str(int(row[1][lst_ts_labels[4]]))]['1']).ravel()
    
    #CAS 2018-02-17
    #for the daily MSD runs, level ts span 00:00 through 00:00 of the 
    #next day, while the overflow spans 00:05 through 00:00 of the next day.
    #Therefore, unequal array length and can't be combined as is into matrix
    #For all level ts, chop out the first element to match overflow
    #Continuous appending of this data format will provide continuous data
    #with no gaps or overlaps
    y1 = y1[1:len(y1)]  #   !!! this clips out the first timestep, so the start_date time should be set to 00:05 when using this
    y2 = y2[1:len(y2)]
    y3 = y3[1:len(y3)]
    y5 = y5[1:len(y5)]
    
    y1_norm = normalize_data(y1)
    y2_norm = normalize_data(y2)
    y3_norm = normalize_data(y3)
    y4_norm = normalize_data(y4)

    #create label and datetime lists of same array length
    label = [row[1][0]]*len(y1)  # create a list of the label, repeating
    datelist = pd.date_range(start_date, periods=len(y1),freq= (str(ts_min*60)+"s")).tolist() #create datetime list;
    date_strings = [str(dt) for dt in datelist] #use map
    
    #combine data into df and return
    df = pd.DataFrame(np.column_stack((label,date_strings,y1,y2,y3,y4,y1_norm,y2_norm,y3_norm,y4_norm, y5)))
    df.columns = cols_out
        
    return df


# redistribute the data accordint to a normalizer (http://scikit-learn.org/stable/auto_examples/preprocessing/plot_all_scaling.html#sphx-glr-auto-examples-preprocessing-plot-all-scaling-py)
# condisder standard scaler, normalizer, or quantile transformer...
def normalize_data(my_series):
    
    #reshape array (based on error message 2018/01/22)
    my_series = my_series.reshape(-1, 1)
    
    scaler = StandardScaler(copy=True, with_mean=True)
    scaler.fit(my_series)
    y_return = scaler.transform(my_series)

    return y_return
    

class msd_scenario:
    def __init__(self, fp):
        self.fp = fp
        self.scenID = int(os.path.basename(fp))
        self.simDate = get_start_date(self.fp)
        self.runDate = get_run_date(self.fp)
        self.error = ""
    
    def get_log(self):
        return "Scenario {} ({}) run on {}. Notes: {}".format(self.scenID, self.simDate, self.runDate, self.error)
    
    def reveal(self):
        print(self.get_log())
        
        
class msd_scen_dict:
    
    def __init__(self):
        self.scen_dict = dict()
        self.run_dict = dict()
        self.error_log = []
        
    
    def update_dict(self, msd_scenario):
        
        if msd_scenario.simDate not in self.scen_dict.keys():
            
            self.scen_dict[msd_scenario.simDate] = msd_scenario.scenID
            self.run_dict[msd_scenario.simDate] = msd_scenario.runDate
        else: 
            
            if(msd_scenario.runDate > self.run_dict[msd_scenario.simDate]):
                self.scen_dict[msd_scenario.simDate] = msd_scenario.scenID
                self.run_dict[msd_scenario.simDate] = msd_scenario.runDate
                
    def update_log(self, text):
        self.error_log.append(text)
        
    def write_summary_table(self, fp_out):
        scen_df = pd.DataFrame.from_dict(self.scen_dict, orient = "index")
        run_df = pd.DataFrame.from_dict(self.run_dict, orient = "index")
        scen_df.columns = ["ScenID"]
        run_df.columns = ["LastRunDate"]
        df = scen_df.join(run_df, how = "outer")
        df = df.sort_index()
        df.index.name = "SimDate"
        df.to_csv(fp_out)
        
        
    def write_error_log(self):
        return 

