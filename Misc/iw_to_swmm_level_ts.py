# -*- coding: utf-8 -*-
"""
Created on Wed Oct 04 11:30:06 2017

@author: CS029534
"""

fp_lvl_in = r'F:\proj\Analysis\OptimaticsOptimization\ModelConversion\Area3ABC\Level\Area3ABC_IWLEVEL.csv'
fp_lvl_out = r'F:\proj\Analysis\OptimaticsOptimization\ModelConversion\Area3ABC\Level\Area3ABC_LevelTS.txt'

fp_outfalls_in = r'F:\proj\Analysis\OptimaticsOptimization\ModelConversion\Area3ABC\Level\Area3ABC_outfall_list.txt'
fp_outfalls_out = r'F:\proj\Analysis\OptimaticsOptimization\ModelConversion\Area3ABC\Level\Area3ABC_OUTFALLS.txt'

import re
import os
import numpy as np
import pandas as pd

node_list_marker = 'L_NODEID'
ts_data_marker = 'P_DATETIME'

#helper function to find the row number containing a marker within the file
def find_row(df, marker):
    nrows = df.shape[0]
    for i in range(nrows):
        ismatch = df.iloc[i,0].startswith(marker)
        if ismatch:
            return i

#read in as single column text string, find section breaks
df = pd.read_table(fp_lvl_in)
si_nodelist = find_row(df, node_list_marker) + 1
fi_nodelist = find_row(df, ts_data_marker)
si_leveldata = fi_nodelist + 1

#extract node list
nodelist_section = df.iloc[si_nodelist:fi_nodelist, 0]
nodes = [x.split(',')[0].strip() for x in nodelist_section]
node_set = set(nodes)

#extract level data ts data frame
data_section = df.iloc[si_leveldata:, 0]
df = pd.DataFrame([s.split(',') for s in data_section])
colnames = [ts_data_marker] + nodes
df.columns = colnames
nrows = df.shape[0]

#get set of existing outfall nodes from outfall list
outfalls = pd.read_table(fp_outfalls_in, sep= ' ', header = None)
of_set = set(outfalls.iloc[:, 0])
numofs = outfalls.shape[0]
outfalls['4'] = ['']*(numofs) #add column with empty strings

obj = []
dts = []
tms= []
val = []

for ni, n in enumerate(nodes):
    
    if n not in of_set: #skip nodes that are not outfalls in current model
        continue
    
    for i in range(nrows):
        
        obj.append('LVL_' + n)
        dts.append(r'1/1/2001')
        tms.append(df.iloc[i, 0].split()[1] + ':00')
        val.append(df.iloc[i, ni+1])
        
df_out = pd.DataFrame(zip(obj, dts, tms, val))
df_out.to_csv(fp_lvl_out, sep = ' ', index = False, header = False)


#create update the outfalls table

for i in range(numofs):
    cnode = outfalls.iloc[i,0]
    if cnode in node_set:
        outfalls.iloc[i,4] = outfalls.iloc[i,3]
        outfalls.iloc[i,3] = 'LVL_' + cnode
        outfalls.iloc[i,2] = 'TIMESERIES'
    else:
        outfalls.iloc[i,4] = outfalls.iloc[i,3]
        outfalls.iloc[i,3] = ''
        
outfalls.to_csv(fp_outfalls_out, sep = ' ', index = False, header = False)
