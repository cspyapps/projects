# -*- coding: utf-8 -*-
"""
Created on Wed Sep 27 12:22:07 2017

@author: CS029534
"""
import pandas as pd
import re

df = pd.read_csv('swmm_shape_arrays.csv')

shape_list = []
type_list = []
height_list= []
width_list = []


nrows = df.shape[0]

for i in range(nrows):
    df.iloc[i, 3] = re.sub("{|}", "", df.iloc[i,3])
    df.iloc[i, 4] = re.sub("{|}", "", df.iloc[i,4])
    
    harray = map(float, df.iloc[i,3].split(","))
    warray = map(float, df.iloc[i,4].split(","))
    
    for e in range(len(harray)):
        
        shape_list.append(df.iloc[i,0])
        if e == 0:
            type_list.append("SHAPE")
        else:
            type_list.append("")
        height_list.append(harray[e])
        width_list.append(warray[e]*df.iloc[i,2])
        

df_out = pd.DataFrame(zip(shape_list, type_list, height_list, width_list))

df_out.to_csv('swmm_shape_arrays_converted.csv', index= False)
