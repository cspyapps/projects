# -*- coding: utf-8 -*-
"""
Created on Wed Sep 13 19:41:09 2017

@author: Craig
"""

# -----------------
# User Instructions
# 
# Write a function, subway, that takes lines as input (read more about
# the **lines notation in the instructor comments box below) and returns
# a dictionary of the form {station:{neighbor:line, ...}, ... } 
#
# For example, when calling subway(boston), one of the entries in the 
# resulting dictionary should be 'foresthills': {'backbay': 'orange'}. 
# This means that foresthills only has one neighbor ('backbay') and 
# that neighbor is on the orange line. Other stations have more neighbors:
# 'state', for example, has 4 neighbors.
#
# Once you've defined your subway function, you can define a ride and 
# longest_ride function. ride(here, there, system) takes as input 
# a starting station (here), a destination station (there), and a subway
# system and returns the shortest path.
#
# longest_ride(system) returns the longest possible ride in a given 
# subway system. 

# -------------
# Grading Notes
#
# The subway() function will not be tested directly, only ride() and 
# longest_ride() will be explicitly tested. If your code passes the 
# assert statements in test_ride(), it should be marked correct.

def subway(**lines):
    """Define a subway map. Input is subway(linename='station1 station2...'...).
    Convert that and return a dict of the form: {station:{neighbor:line,...},...}"""
    
    keys = lines.keys()
    raw_values = lines.values()
    stations = set()
    mapping = {}
    
    #extract string to list, set
    for i,k in enumerate(keys):
        line_stations = raw_values[i].split(' ')
        lines[k] = line_stations
        for s in line_stations:
            stations.add(s)
    
    for s in stations:
        mapping[s] = {}
    
    #loop and find neighbors
    for station in stations:
        for line in keys:
            
            line_list = lines[line]
            if station not in line_list:
                continue
            else:
                neighbors = get_neighbors(station, line_list)
                
                for n in neighbors:
                    mapping[station][n] = line
    
    return(mapping)
                
                
                
def get_neighbors(string, iterable):
    
    if len(iterable) == 0 or iterable == None:
        return None
    else:
        max_index = len(iterable) - 1
    
    if string not in iterable:
        return None
    else:
        pos = [i for i,x in enumerate(iterable) if x == string]
    
    #for our case, there can only be one appearance per list. take first
    pos = pos[0]
    
    n1 = iterable[pos-1] if (pos-1 >= 0) else None
    n2 = iterable[pos+1] if (pos+1 <= max_index) else None
    
    if n1 != None and n2 != None:
        return((n1, n2))
    elif n1 != None and n2 == None:
        return ((n1,)) #comma enforces that a tuple is return, not string
    else: 
        return((n2,))
    
    
boston = subway(
    blue='bowdoin government state aquarium maverick airport suffolk revere wonderland',
    orange='oakgrove sullivan haymarket state downtown chinatown tufts backbay foresthills',
    green='lechmere science north haymarket government park copley kenmore newton riverside',
    red='alewife davis porter harvard central mit charles park downtown south umass mattapan')

def ride(here, there, system=boston):
    "Return a path on the subway system from here to there."
    
    def successors(x):
        return system[x]
    
    def is_goal(y):
            return True if y == there else False
    
    return shortest_path_search(here, successors, is_goal)
    

def longest_ride(system):
    """"Return the longest possible 'shortest path' 
    ride between any two stops in the system."""
    longest_path = []
    stations = system.keys()
    for h in stations:
        for t in stations:
            path = ride(h, t, system)
            if len(path_states(path)) > len(longest_path):
                longest_path = path
    return longest_path
                

def shortest_path_search(start, successors, is_goal):
    """Find the shortest path from start state to a state
    such that is_goal(state) is true."""
    if is_goal(start):
        return [start]
    explored = set() # set of states we have visited
    frontier = [ [start] ] # ordered list of paths we have blazed
    while frontier:
        path = frontier.pop(0)
        s = path[-1]
        for (state, action) in successors(s).items():
            if state not in explored:
                explored.add(state)
                path2 = path + [action, state]
                if is_goal(state):
                    return path2
                else:
                    frontier.append(path2)
    return []

def path_states(path):
    "Return a list of states in this path."
    return path[0::2]
    
def path_actions(path):
    "Return a list of actions in this path."
    return path[1::2]

def test_ride():
    assert ride('mit', 'government') == [
        'mit', 'red', 'charles', 'red', 'park', 'green', 'government']
    assert ride('mattapan', 'foresthills') == [
        'mattapan', 'red', 'umass', 'red', 'south', 'red', 'downtown',
        'orange', 'chinatown', 'orange', 'tufts', 'orange', 'backbay', 'orange', 'foresthills']
    assert ride('newton', 'alewife') == [
        'newton', 'green', 'kenmore', 'green', 'copley', 'green', 'park', 'red', 'charles', 'red',
        'mit', 'red', 'central', 'red', 'harvard', 'red', 'porter', 'red', 'davis', 'red', 'alewife']
    assert (path_states(longest_ride(boston)) == [
        'wonderland', 'revere', 'suffolk', 'airport', 'maverick', 'aquarium', 'state', 'downtown', 'park',
        'charles', 'mit', 'central', 'harvard', 'porter', 'davis', 'alewife'] or 
        path_states(longest_ride(boston)) == [
                'alewife', 'davis', 'porter', 'harvard', 'central', 'mit', 'charles', 
                'park', 'downtown', 'state', 'aquarium', 'maverick', 'airport', 'suffolk', 'revere', 'wonderland'])
    assert len(path_states(longest_ride(boston))) == 16
    return 'test_ride passes'

print test_ride()