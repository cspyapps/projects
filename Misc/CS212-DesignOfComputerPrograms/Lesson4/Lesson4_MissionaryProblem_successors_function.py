# -*- coding: utf-8 -*-
"""
Created on Sun Sep 10 11:40:49 2017

@author: Craig
"""

# -----------------
# User Instructions
# 
# Write a function, csuccessors, that takes a state (as defined below) 
# as input and returns a dictionary of {state:action} pairs. 
#
# A state is a tuple with six entries: (M1, C1, B1, M2, C2, B2), where 
# M1 means 'number of missionaries on the left side.'
#
# An action is one of the following ten strings: 
#
# 'MM->', 'MC->', 'CC->', 'M->', 'C->', '<-MM', '<-MC', '<-M', '<-C', '<-CC'
# where 'MM->' means two missionaries travel to the right side.
# 
# We should generate successor states that include more cannibals than
# missionaries, but such a state should generate no successors.

def csuccessors(state):
    """Find successors (including those that result in dining) to this
    state. But a state where the cannibals can dine has no successors."""
    M1, C1, B1, M2, C2, B2 = state
    
    
    sdict = dict()
    
    if C1 > M1 or C2 > M2:
        return(sdict)
    
    if B1 == 1:
        for m in range(M1+1):
            for c in range(C1+1):
                if m+c == 0 or m+c > 2:
                    continue
                else:
                    sM1 = M1 - m
                    sC1 = C1 - c
                    sB1 = B2
                    sM2 = M2 + m
                    sC2 = C2 + c
                    sB2 = B1
                    sstate = (sM1, sC1, sB1, sM2, sC2, sB2)
                    action = 'M'*m+'C'*c+'->'
                    sdict[sstate] = action
    
    else:
        for m in range(M2+1):
            for c in range(C2+1):
                if m+c == 0 or m+c > 2:
                    continue
                else:
                    sM2 = M2 - m
                    sC2 = C2 - c
                    sB2 = B1
                    sM1 = M1 + m
                    sC1 = C1 + c
                    sB1 = B2
                    sstate = (sM1, sC1, sB1, sM2, sC2, sB2)
                    action = '<-'+'M'*m+'C'*c
                    sdict[sstate] = action
    
    return(sdict)
                    
                    
                    



def test():
    assert csuccessors((2, 2, 1, 0, 0, 0)) == {(2, 1, 0, 0, 1, 1): 'C->', 
                                               (1, 2, 0, 1, 0, 1): 'M->', 
                                               (0, 2, 0, 2, 0, 1): 'MM->', 
                                               (1, 1, 0, 1, 1, 1): 'MC->', 
                                               (2, 0, 0, 0, 2, 1): 'CC->'}
    assert csuccessors((1, 1, 0, 4, 3, 1)) == {(1, 2, 1, 4, 2, 0): '<-C', 
                                               (2, 1, 1, 3, 3, 0): '<-M', 
                                               (3, 1, 1, 2, 3, 0): '<-MM', 
                                               (1, 3, 1, 4, 1, 0): '<-CC', 
                                               (2, 2, 1, 3, 2, 0): '<-MC'}
    assert csuccessors((1, 4, 1, 2, 2, 0)) == {}
    return 'tests pass'

print test()