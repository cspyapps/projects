# -*- coding: utf-8 -*-
"""
Created on Sun Aug 27 17:18:40 2017

@author: Craig
"""

def longest_subpalindrome_slice(text):
    "Return (i, j) such that text[i:j] is the longest palindrome in text."
    text = text.lower()
    strlen = len(text)
    if strlen == 0:
        return (0, 0)
    
    f = lambda x: x[1] - x[0]
    subpals_indx = (0, 0)
    
    for i in range(strlen):
        for j in range(strlen+1):
            if j <= i or f((i, j)) <= f(subpals_indx):
                next
            else: 
                if text[i:j] == text[i:j][::-1]:
                    subpals_indx = (i, j)
                
    return subpals_indx
    
    
def test():
    L = longest_subpalindrome_slice
    assert L('panda')
    assert L('racecar') == (0, 7)
    assert L('Racecar') == (0, 7)
    assert L('RacecarX') == (0, 7)
    assert L('Race carr') == (7, 9)
    assert L('') == (0, 0)
    assert L('something rac e car going') == (8,21)
    assert L('xxxxx') == (0, 5)
    assert L('Mad am I ma dam.') == (0, 15)
    return 'tests pass'

print test()



def countmeup(string):
    n = len(string)
    thelist = []
    countmeup.counter = 0
    for i in range(n):
        for j in range(n+1):
            if j-i >= 1:
                thelist.append(string[i:j])
                countmeup.counter += 1
    return(thelist)