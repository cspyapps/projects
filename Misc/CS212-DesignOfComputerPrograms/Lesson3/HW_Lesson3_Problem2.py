# -*- coding: utf-8 -*-
"""
Created on Wed Sep 06 19:27:27 2017

@author: Craig
"""

def slow_inverse(f, delta=1/128.):
    """Given a function y = f(x) that is a monotonically increasing function on
    non-negatve numbers, return the function x = f_1(y) that is an approximate
    inverse, picking the closest value to the inverse, within delta."""
    def f_1(y):
        x = 0
        while f(x) < y:
            x += delta
        # Now x is too big, x-delta is too small; pick the closest to y
        return x if (f(x)-y < y-f(x-delta)) else x-delta
    return f_1 

def inverse(f, delta = 1/128.):
    """Given a function y = f(x) that is a monotonically increasing function on
    non-negatve numbers, return the function x = f_1(y) that is an approximate
    inverse, picking the closest value to the inverse, within delta."""
    
    """
    Peter's solution used a binary search but discovered the first set
    of bounds by starting at the bounds of (0, delta), and progressively doubling
    the upper bound until the f(x) > y, then implementing the binary search
    halfing algorithm
    
    """
    
    def f_1(y, tol = 1/128.):
        x_lower = 0
        x_upper = 1e18 #arbitrary upper limit on search range
        x_guess = 1e9 #arbitrary initial guess
        
        while abs(f(x_guess) - y) > tol:
            
            if f(x_guess) - y > 0:
                x_upper = x_guess
                x_guess = (x_lower + x_guess)/2
                
            if f(x_guess) - y < 0:
                x_lower = x_guess
                x_guess = (x_upper + x_guess)/2
                
                
        return(x_guess)
    
    return(f_1)
            
    
def square(x): return x*x
sqrt = inverse(square)

print sqrt(1000000000)
