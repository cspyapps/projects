# -*- coding: utf-8 -*-
"""
Created on Mon Sep 04 17:13:15 2017

@author: Craig
"""

def n_ary(f):
    """Given binary function f(x, y), return an n_ary function such
    that f(x, y, z) = f(x, f(y,z)), etc. Also allow f(x) = x."""
    def n_ary_f(x, *args):
        nargs = len(args)
        if nargs == 0:
            return(f(x))
        else:
            string = 'f(x, '
            for a in args[:nargs-1]:
                string = string + 'f(' + str(a) + ', '
            string = string + str(args[nargs-1]) + ')'*(nargs)
        print(string)
        return(eval(string))
    return n_ary_f


def add(x, y):
    return x+y


n_add = n_ary(add)

print(n_add(1,2,3,4,5,6))


#Peter's solution:
    
def n_ary_f(x, *args):
        return x if not args else f(x, n_ary_f(*args))
    return n_ary_f

