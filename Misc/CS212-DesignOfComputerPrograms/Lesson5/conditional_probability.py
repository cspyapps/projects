# -*- coding: utf-8 -*-
"""
Created on Sun Sep 17 14:58:24 2017

@author: Craig
"""

import itertools

sex = 'BG'
day = 'SMTWtFs'

def product(*variables):
    "The cartesian product (as a str) of the possibilities of each variable"
    return map(''.join, itertools.product(*variables))


x = product(sex, day, sex, day)

B = [s for s in x if 'BT' in s]
A = [i for i in B if i[0] == 'B' and i[2] == 'B']

print("Conditional prob = {}".format(float(len(A))/float(len(B))))



def find_fraction(dec, tol = 0.000001):
    for n in range(100):
        for d in range(100):
            if n == 0 or d == 0:
                continue
            if abs((float(n)/float(d)) - dec) < tol:
                return((n, d))
