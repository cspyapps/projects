# -*- coding: utf-8 -*-
"""
Created on Sat Sep 23 11:01:57 2017

@author: Craig
"""

def a_board():
    return map(list, ['|||||||||||||||||',
                      '|J............I.|',
                      '|A.....BE.C...D.|',
                      '|GUY....F.H...L.|',
                      '|||||||||||||||||'])

def show(board):
    "Print the board."
    for i in board:
        print(''.join(i) + '\n')

show(a_board())

hplays = {((10, 13), 'great'), ((7, 2), 'gnarly')}

hplays2 = {(x[0], 'ACROSS', x[1]) for x in hplays}
