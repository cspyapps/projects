# -*- coding: utf-8 -*-
"""
Created on Fri Dec 01 08:31:20 2017

@author: CS029534
"""

import re
import os
import numpy as np
import pandas as pd
from datetime import timedelta


#Description
#------------------------------------------------------------------------------
#Aggregates single daily timeseries stored in individual files into a daily CSV 
#files that include all timeseries (presumably different locations). 
#Also aggregates across all days and write a full CSV file (full period, all
#locations) to the last subdirectory
#------------------------------------------------------------------------------
#User Settings
#------------------------------------------------------------------------------
start_ymd = (2018, 6, 5) #(year, month, day)
base_folder = r"\\hchfpp01\Groups\WBG\MillCreek\Model\174" #no trailing backslash
min_scenario = 18355
max_scenario = 18355

fn_regex = None #"CSO-\\d{3}\.dat" #"CSO-\\d{3}-Stream.dat"   #regex for filenames to be aggregated
level_type = "cso" #For MSD project, set to one of ("cso", "stream", "interceptor", "rainfall")

scenario_level_fn_out = "_CSOLevelDAT_Aggregated.csv"   #eg _CSOLevelDAT_Combined.csv
event_level_fn_out = "_CSOLevelDAT_20180605.csv"     #eg _CSOLevelDAT_FullEvent.csv

eval_group_name_dict = {169 : "LittleMiami", 170 : "MuddyCreek", 174 : "MillCreek"}
first_col_name = "tag_names"
ts_min = 5                                #could derive from the existing DAT files
#------------------------------------------------------------------------------


#define helper functions- note that match searches the beginning of the string only
def is_regex_match(regex, string):  
    return True if (re.match(regex, string) != None) else False

def get_ts_from_dat(fp):
    return pd.read_table(fp, header = None)[2]

def get_subfolder_list(base_folder, min_scenario, max_scenario):
    
    sf_all = os.listdir(base_folder) #get all options
    
    #try to parse to int- do not add if error
    sf_candidates = []
    for sf in sf_all:
        try:
            sf_int = int(sf)
            sf_candidates.append(sf_int)
        except:
            a = 1
    #filter based on min/max
    sf_final = [sf for sf in sf_candidates if ((sf>=min_scenario) and (sf<=max_scenario))]
    
    #change back to strings and return
    return list(map(str, sf_final))

def apply_dst_fix(series, index, length):
    lst = series.tolist()
    insert = np.linspace(lst[index], lst[index+1], length)
    l1 = lst[0:(index+1)]
    l2 = lst[(index+1):]
    l1.extend(insert)
    l1.extend(l2)
    return pd.Series(l1)
    
###############################################################################

#get subfolder list
subfolder_list = get_subfolder_list(base_folder, min_scenario, max_scenario)     

#get basin name from the base folder
eval_group_name = eval_group_name_dict[int(base_folder.split("\\")[-1])]

#get fn_regex from the level type string- NOTE: MSD project specific
if fn_regex is None:
    if level_type == "cso":
        fn_regex = "CSO-\\d{3}\.dat"
    elif level_type == "stream":
        fn_regex = "CSO-\\d{3}-Stream.dat"
    elif level_type == "interceptor":
        fn_regex = "CSO-\\d{3}-Interceptor.dat"
    elif level_type == "rainfall":
        fn_regex = "CSO-\\d{3}-Rainfall.dat"
    else:
        raise Exception("level_type must be one of 'cso', 'stream', 'interceptor', or 'rainfall'")

#confirmation
x = input('Operation will aggregate across {} sub-dirs within {}. Press any key to continue: '.format(len(subfolder_list), base_folder))

#pre-allocate the df that spans full event (not just day)
first_dir = base_folder + "\\" + subfolder_list[0]
tsperday = (24*60)/ts_min

tstart = pd.datetime(start_ymd[0], start_ymd[1], start_ymd[2], 0, 0)
tend = tstart + timedelta(days = len(subfolder_list))
tts = pd.date_range(start = tstart, end = tend, freq = str(ts_min)+"min")

tnrow = int(tsperday*len(subfolder_list) + 1)
tncol = int(len([f for f in os.listdir(first_dir) if is_regex_match(fn_regex, f)]) + 1)
tdf = pd.DataFrame(np.empty((tnrow, tncol)))
tdf.iloc[:, 0] = tts

rc = 0
rc2 = tsperday

for day, sf in enumerate(subfolder_list): 
    
    
    cdir = base_folder + "\\" + sf
    files = [f for f in os.listdir(cdir) if is_regex_match(fn_regex, f)]
    
    #build df
    nrow = int(tsperday + 1) #+1 bc midnight through midnight
    ncol = int(len(files) + 1) #+1 bc datetime column
    df = pd.DataFrame(np.empty((nrow, ncol)))
    
    colnames = [first_col_name] + [re.sub("\.dat", "", f) for f in files]
    df.columns = colnames
    tdf.columns = colnames
    
    start = pd.datetime(start_ymd[0], start_ymd[1], start_ymd[2], 0, 0) + pd.DateOffset(days = day)
    end = pd.datetime(start_ymd[0], start_ymd[1], start_ymd[2], 0, 0) + pd.DateOffset(days = day + 1)
    ts = pd.date_range(start = start, end = end, freq = "5min")
    df.iloc[:,0] = ts
    
    print("Working on day {} of {}...".format(day+1, len(subfolder_list)))
    
    for col in colnames:
        
        if col == first_col_name:
            continue 
        
        fp = cdir + "\\" + col + ".dat"
        datts = get_ts_from_dat(fp)
        
        #check and apply DST fix if necessary
        if (start == pd.datetime(2018,3,11)) and (len(datts) == 277):
            datts = apply_dst_fix(datts, 24, 12)
        
        #fill daily dataframe
        df.loc[:,col] = datts
        
        #fill overall (spans full event being aggregated across) dataframe
        ci = int(colnames.index(col))
        tdf.iloc[int(rc):int(rc2+1), ci] = datts.tolist()
        
    rc += tsperday
    rc2 += tsperday #this tallying controls the overwrite of midnight timestamp
    
    #TODO: make the following line more general
    fp_out = cdir + "\\OUT\\" + eval_group_name + scenario_level_fn_out
    df.to_csv(fp_out, index = False)
    
#write full event csv to last subdir
print("Writing full-event aggregated files...")
fp_comb_out = cdir + "\\OUT\\" + eval_group_name + event_level_fn_out
tdf.to_csv(fp_comb_out, index = False)
print("Complete.")
