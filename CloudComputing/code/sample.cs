﻿using Microsoft.Azure.Batch;
using Microsoft.Azure.Batch.Auth;
using Microsoft.Azure.Batch.Common;
using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Blob;
using Microsoft.Azure.Storage.Auth;
using Microsoft.Azure.Batch.Conventions.Files;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;


namespace Surge
{
    public class Program
    {
        private static string version = "v0.1.3"; // Keep in sync with Surge Propject Properties > Package 

        // Update the Batch and Storage account credential strings below with the values unique to your accounts.
        // These are used when constructing connection strings for the Batch and Storage client objects.

        // TODO:  Move all of these settings to a config xml file--------------
        // Batch account credentials
        private const string BatchAccountName = "";
        private const string BatchAccountKey = "";
        private const string BatchAccountUrl = "https://something.centralus.batch.azure.com";

        // Storage account credentials
        private const string StorageAccountName = "test";
        private const string StorageAccountKey = "";

        // Batch resource settings
        private const string PoolId = "AlcosanTesting";
        private const string JobId = "AlcosanTesting";
        private const int lowPriorityNodeCount = 2;
        private const int normalPriorityNodeCount = 0;
        private const string PoolVMSize = "STANDARD_A1_v2"; // use STANDARD_A1_v2 in most cases (2GB RAM, 10GB temp storage, $0.022/hr low and $0.054/hr normal) [https://azure.microsoft.com/en-us/pricing/details/batch/windows-virtual-machines/]

        private const string rootScenDir = @"C:\Users\cs029534\Documents\AzureTesting\244";
        private const int minScenarioID = 24332;
        private const int maxScenarioID = 24333;

        private static string appPkgName = "ALCOSAN_SWMM"; //NOTE: Also need to change the taskCLICommandTemplate at this time!!


        private static List<string> inputFileRegexList = new List<string> { { ".*inp$" } };
        private static List<string> returnFileRegexList = new List<string>() { ".*rpt$" ,".*out$" };


        //TODO:  this needs to be more general- 
        // Assume SWMM for now for CLI command structure, but identify the basename of file(s) on the fly
        // use ~s~ token to represent task/scenario ID
        private const string taskCLICommandTemplate = @"cmd /c %AZ_BATCH_APP_PACKAGE_ALCOSAN_SWMM%\swmm5.exe ~inp~ ~b~.rpt ~b~.out";
            //@"cmd /c %AZ_BATCH_APP_PACKAGE_ALCOSAN_SWMM%\swmm5.exe ~inp~ ~b~.rpt ~b~.out";


        private static TimeSpan timeout = TimeSpan.FromMinutes(120);

        //TODO make use of dictAppPkgs and taskCLICommand in the code body
        // Job-level parameters
        private Dictionary<string, string> dictAppPkgs = new Dictionary<string, string>()
        {
            { "SWMM", "5.1.013" }
        };




        //---------------------------------------------------------------------


        static void Main()
        {

            List<string> targetDirList = GetScenarioFilePaths(rootScenDir, minScenarioID, maxScenarioID);
            List<string> inputFilePaths = GetInputFilePathList(targetDirList, inputFileRegexList);
            int nTasks = targetDirList.Count;
            int nInputFiles = inputFilePaths.Count;

            // TODO: Read in settings from xml 

            if (String.IsNullOrEmpty(BatchAccountName) ||
                String.IsNullOrEmpty(BatchAccountKey) ||
                String.IsNullOrEmpty(BatchAccountUrl) ||
                String.IsNullOrEmpty(StorageAccountName) ||
                String.IsNullOrEmpty(StorageAccountKey))
            {
                throw new InvalidOperationException("One or more account credential strings have " +
                    "not been populated. Please ensure that your Batch and Storage account credentials " +
                    "have been specified.");
            }

            // Create Azure Batch Storage Credentials object for convenience
            Microsoft.WindowsAzure.Storage.Auth.StorageCredentials storageCreds_windows =
                new Microsoft.WindowsAzure.Storage.Auth.StorageCredentials(StorageAccountName, StorageAccountKey);

            BatchSharedKeyCredentials batchCreds = new BatchSharedKeyCredentials(BatchAccountUrl, BatchAccountName, BatchAccountKey);


            try
            {
                Console.WriteLine("<-------------- Welcome to SURGE {0} ------------->", version);
                Console.WriteLine();
                Console.WriteLine("Initiating at: {0}", DateTime.Now);
                Console.WriteLine();
                Stopwatch timer = new Stopwatch();
                timer.Start();

                // Create the blob client, for use in obtaining references to blob storage containers
                CloudBlobClient blobClient = CreateCloudBlobClient(StorageAccountName, StorageAccountKey);

                // Use the blob client to create the input container in Azure Storage 
                string inputContainerName = string.Format("job-{0}", JobId.ToLower());

                CloudBlobContainer container = blobClient.GetContainerReference(inputContainerName);

                container.CreateIfNotExistsAsync().Wait();

                // Upload the data files to Azure Storage. This is the data that will be processed by each of the tasks that are
                // executed on the compute nodes within the pool.
                List<ResourceFile> inputFiles = new List<ResourceFile>();
                List<int> lstScenarioIDs = new List<int>();

                Console.WriteLine("Uploading {0} files for {1} tasks to container {2}...", 
                    nInputFiles, nTasks, inputContainerName);

                foreach (string filePath in inputFilePaths)
                {
                    inputFiles.Add(UploadFileToContainer(blobClient, inputContainerName, filePath));
                    lstScenarioIDs.Add(GetScenarioIDFromFilePath(filePath));  //TODO: Update method implementation to grab from Scenario folder name
                }

                using (BatchClient batchClient = BatchClient.Open(batchCreds))
                {
                    Console.WriteLine("Creating pool [{0}]...", PoolId);

                    // Create a Windows Server image, VM configuration, Batch pool
                    ImageReference imageReference = CreateImageReference();

                    VirtualMachineConfiguration vmConfiguration = CreateVirtualMachineConfiguration(imageReference);

                    CreateBatchPool(batchClient, vmConfiguration);

                    // Create a Batch job
                    Console.WriteLine("Creating job [{0}]...", JobId);

                    // Declare linkedStorageAccount var
                    Microsoft.WindowsAzure.Storage.CloudStorageAccount linkedStorageAccount =
                        new Microsoft.WindowsAzure.Storage.CloudStorageAccount(storageCreds_windows, true);

                    try
                    {
                        CloudJob job = batchClient.JobOperations.CreateJob();
                        job.Id = JobId;
                        job.PoolInformation = new PoolInformation { PoolId = PoolId };

                        // NEW CODE RELATED TO OUTPUT FILES (2019-08-01)
                        // What is the difference between Microsoft.WindowsAzure and Microsoft.Azure libraries?  Seeing
                        // some errors relating to problems convert types between these two below
                        job.PrepareOutputStorageAsync(linkedStorageAccount);
                        job.Commit();
                    }
                    catch (BatchException be)
                    {
                        // Accept the specific error code JobExists as that is expected if the job already exists
                        if (be.RequestInformation?.BatchError?.Code == BatchErrorCodeStrings.JobExists)
                        {
                            Console.WriteLine("The job {0} already existed when we tried to create it", JobId);
                        }
                        else
                        {
                            throw; // Any other exception is unexpected
                        }
                    }

                    #region DEFINE_TASKS
                    // Create a collection to hold the tasks that we'll be adding to the job
                    Console.WriteLine("Adding {0} tasks to job [{1}]...", nTasks, JobId);
                    Console.WriteLine();

                    List<CloudTask> tasks = new List<CloudTask>();

                    // Create each of the tasks to process one of the input files.
                    for (int i = 0; i < nTasks; i++)
                    {
                        int scenarioID = lstScenarioIDs[i];
                        string taskId = String.Format("Task{0}", scenarioID);

                        // The inp filepath referenced in the task CLI command is the filepath local to the machine running
                        // this application. However, Azure Batch only uses this to identify the appropriate Resource File
                        // in the linked Azure Storage account. 

                        // BELOW IS SWMM-SPECIFIC!!
                        // Only works beacuse there is one input file (inp) per task
                        // This needs to be generalized to allow for SWMM runs with external files and other models/workloads
                        string taskCmd = UpdateTaskCLICommand(taskCLICommandTemplate, "~b~", GetBaseFilenameFromFilePath(inputFilePaths[i]));
                        taskCmd = UpdateTaskCLICommand(taskCmd, "~inp~", inputFilePaths[i]);

                        // REMOVEME
                        // Create output storage object
                        /*
                        string outputFileName = string.Format("wd\test{0}.out", scenarioID);
                        string reportFileName = string.Format("wd\test{0}.rpt", scenarioID);

                        Microsoft.WindowsAzure.Storage.CloudStorageAccount outputStorageAccountRef =
                            new Microsoft.WindowsAzure.Storage.CloudStorageAccount(storageCreds_windows, true);


                        TaskOutputStorage taskOutputStorage = new TaskOutputStorage(outputStorageAccountRef, JobId, taskId);
                        taskOutputStorage.SaveAsync(TaskOutputKind.TaskOutput, "stdout.txt");
                        taskOutputStorage.SaveAsync(TaskOutputKind.TaskOutput, outputFileName);
                        taskOutputStorage.SaveAsync(TaskOutputKind.TaskOutput, reportFileName);
                        */


                        CloudTask task = new CloudTask(taskId, taskCmd);
                        task.ResourceFiles = new List<ResourceFile> { inputFiles[i] };  // Add the relevant resource file references to the task
                        tasks.Add(task);

                        if(i == 0) // Only print the first one
                        {
                            Console.WriteLine(string.Format("    Created task {0} with command: {1}", task.Id, taskCmd));
                            Console.WriteLine(string.Format("and {0} other similar tasks", nTasks - 1));
                            Console.WriteLine();
                        }
                    }
                    #endregion

                    // Add all tasks to the job.
                    batchClient.JobOperations.AddTask(JobId, tasks);


                    // Monitor task success/failure, specifying a maximum amount of time to wait for the tasks to complete.

                    Console.WriteLine("Monitoring all tasks for 'Completed' state (auto timeout in {0})...", timeout);

                    IEnumerable<CloudTask> addedTasks = batchClient.JobOperations.ListTasks(JobId);

                    batchClient.Utilities.CreateTaskStateMonitor().WaitAll(addedTasks, TaskState.Completed, timeout);

                    Console.WriteLine("All tasks reached 'Completed' state.");
                    Console.WriteLine("Retrieving target output files...");
                    Console.WriteLine();

                    IEnumerable<CloudTask> completedtasks = batchClient.JobOperations.ListTasks(JobId);


                    int nOutFileCount = 0;
                    List<string> returnVMFilePathList = new List<string>();

                    // Get output files
                    foreach (CloudTask task in completedtasks)
                    {

                        // Get a list of all filepaths on the node
                        IEnumerable<NodeFile> lstNodeFiles = task.ListNodeFiles(recursive: true);

                        // Build the output directory for the task
                        string outDir = Path.Join(rootScenDir, GetScenarioIDFromTaskID(task.Id).ToString());

                        foreach (NodeFile nf in lstNodeFiles)
                        {

                            if ((bool)nf.IsDirectory)
                                continue; // Skip directories

                            string vmFilePath = nf.Path;

                            foreach(string strrx in returnFileRegexList)
                            {
                                Regex rx = new Regex(strrx);
                                if ((rx.IsMatch(vmFilePath)) && !(returnVMFilePathList.Contains(vmFilePath)))  // Match and not already downloaded
                                {
                                    string fpOut = Path.Join(outDir, Path.GetFileName(vmFilePath)); // Build local destination fp
                                    using (var fileStream = File.Create(fpOut))
                                    {
                                        Stream mStream = new MemoryStream();
                                        nf.CopyToStream(mStream);
                                        mStream.Seek(0, SeekOrigin.Begin);
                                        mStream.CopyTo(fileStream);
                                    }
                                    nOutFileCount++;
                                }
                            }
                        }
                    }

                    //nodeFile.CopyToStream()

                    /*
                    foreach (OutputFileReference output in
                        task.OutputStorage(linkedStorageAccount).ListOutputs(
                            TaskOutputKind.TaskOutput))
                    {
                        Console.WriteLine($"output file: {output.FilePath}");

                        output.DownloadToFileAsync(
                            $"{JobId}-{output.FilePath}",
                            System.IO.FileMode.Create).Wait();
                    }
                    */

                    /*
                    // TODO: Need to figure out how to programatically download the output files to local machine
                    // Are these only saved to the compute node and lost when the pool is deleted?
                    // Do we need to explicitly copy to the blob storage prior to downloading locally?
                    // Start by digging into "download the files programmatically" statement here: https://docs.microsoft.com/en-us/azure/batch/tutorial-parallel-dotnet
                    // And examples here: https://github.com/Azure-Samples/azure-batch-samples/tree/master/CSharp
                    foreach (NodeFile nf in task.ListNodeFiles())
                    {
                        Console.WriteLine("    Showing contents of file {0}:", nf.Path);
                        Console.WriteLine(string.Format("    {0}", task.GetNodeFile(nf.Path).ReadAsString()));
                    }
                    */

                    // Print out some timing info
                    timer.Stop();
                    Console.WriteLine();
                    Console.WriteLine("Downloaded {0} files successfully.", nOutFileCount);
                    Console.WriteLine("Finalizing at: {0}", DateTime.Now);
                    Console.WriteLine("Total Elapsed Time: {0}", timer.Elapsed);

                    // Clean up Storage resources
                    Console.WriteLine();
                    Console.Write("Delete blob container? [y]/n: ");
                    string response = Console.ReadLine().ToLower();
                    if (response != "n" && response != "no")
                    {
                        container.DeleteIfExistsAsync().Wait();
                        Console.WriteLine("    Container [{0}] deleted.", inputContainerName);
                    }

                    // Clean up Batch resources (if the user so chooses)
                    Console.WriteLine();
                    Console.Write("Delete job? [y]/n: ");
                    response = Console.ReadLine().ToLower();
                    if (response != "n" && response != "no")
                    {
                        batchClient.JobOperations.DeleteJob(JobId);
                        Console.WriteLine("    Job [{0}] deleted.", JobId);
                    }

                    Console.Write("Delete pool? [y]/n: ");
                    response = Console.ReadLine().ToLower();
                    if (response != "n" && response != "no")
                    {
                        batchClient.PoolOperations.DeletePool(PoolId);
                        Console.WriteLine("    Pool [{0}] deleted.", PoolId);
                    }
                }
            }

            finally
            {
                Console.WriteLine();
                Console.WriteLine("SURGE operation completed, hit ENTER to exit...");
                Console.ReadLine();
            }

        }

        private static void CreateBatchPool(BatchClient batchClient, VirtualMachineConfiguration vmConfiguration)
        {
            try
            {
                CloudPool pool = batchClient.PoolOperations.CreatePool(
                    poolId: PoolId,
                    targetLowPriorityComputeNodes: lowPriorityNodeCount,
                    targetDedicatedComputeNodes: normalPriorityNodeCount,
                    virtualMachineSize: PoolVMSize,
                    virtualMachineConfiguration: vmConfiguration);

                // From https://docs.microsoft.com/en-us/azure/batch/batch-application-packages
                // "Install applications on compute nodes" section
                pool.ApplicationPackageReferences = new List<ApplicationPackageReference>
                {
                    new ApplicationPackageReference {
                        ApplicationId = appPkgName }
                        // Version = "5.1.013"} // assume using default for now

                };

                pool.Commit();
            }
            catch (BatchException be)
            {
                // Accept the specific error code PoolExists as that is expected if the pool already exists
                if (be.RequestInformation?.BatchError?.Code == BatchErrorCodeStrings.PoolExists)
                {
                    Console.WriteLine("The pool {0} already existed when we tried to create it", PoolId);
                }
                else
                {
                    throw; // Any other exception is unexpected
                }
            }
        }

        private static VirtualMachineConfiguration CreateVirtualMachineConfiguration(ImageReference imageReference)
        {
            return new VirtualMachineConfiguration(
                imageReference: imageReference,
                nodeAgentSkuId: "batch.node.windows amd64");
        }

        private static ImageReference CreateImageReference()
        {
            return new ImageReference(
                publisher: "MicrosoftWindowsServer",
                offer: "WindowsServer",
                sku: "2016-datacenter-smalldisk",
                version: "latest");
        }

        /// <summary>
        /// Creates a blob client
        /// </summary>
        /// <param name="storageAccountName">The name of the Storage Account</param>
        /// <param name="storageAccountKey">The key of the Storage Account</param>
        /// <returns></returns>
        private static CloudBlobClient CreateCloudBlobClient(string storageAccountName, string storageAccountKey)
        {
            // Construct the Storage account connection string
            string storageConnectionString =
                $"DefaultEndpointsProtocol=https;AccountName={storageAccountName};AccountKey={storageAccountKey}";

            // Retrieve the storage account
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(storageConnectionString);

            // Create the blob client
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            return blobClient;
        }


        /// <summary>
        /// Uploads the specified file to the specified Blob container.
        /// </summary>
        /// <param name="blobClient">A <see cref="CloudBlobClient"/>.</param>
        /// <param name="containerName">The name of the blob storage container to which the file should be uploaded.</param>
        /// <param name="filePath">The full path to the file to upload to Storage.</param>
        /// <returns>A ResourceFile instance representing the file within blob storage.</returns>
        private static ResourceFile UploadFileToContainer(CloudBlobClient blobClient, string containerName, string filePath)
        {
            //Console.WriteLine("Uploading file {0} to container [{1}]...", filePath, containerName);

            string blobName = Path.GetFileName(filePath);

            filePath = Path.Combine(Environment.CurrentDirectory, filePath);

            CloudBlobContainer container = blobClient.GetContainerReference(containerName);
            CloudBlockBlob blobData = container.GetBlockBlobReference(blobName);
            blobData.UploadFromFileAsync(filePath).Wait();

            // Set the expiry time and permissions for the blob shared access signature. 
            // In this case, no start time is specified, so the shared access signature 
            // becomes valid immediately
            SharedAccessBlobPolicy sasConstraints = new SharedAccessBlobPolicy
            {
                SharedAccessExpiryTime = DateTime.UtcNow.AddHours(2),
                Permissions = SharedAccessBlobPermissions.Read
            };

            // Construct the SAS URL for blob
            string sasBlobToken = blobData.GetSharedAccessSignature(sasConstraints);
            string blobSasUri = String.Format("{0}{1}", blobData.Uri, sasBlobToken);

            return ResourceFile.FromUrl(blobSasUri, filePath);
        }


        /// <summary>
        /// Updates the CLI commmand task template string to reflect the target scenario ID
        /// </summary>
        /// <param name="taskCLICommandTemplate">Generic CLI command for all tasks, where the scenario ID is referenced by "~s~"</param>
        /// <param name="scenarioID">Scenario ID</param>
        /// <returns></returns>
        private static string UpdateTaskCLICommand(string taskCLICommandTemplate, int scenarioID)
        {
            return taskCLICommandTemplate.Replace("~s~", scenarioID.ToString());
        }


        /// <summary>
        /// Generic overload for above method
        /// </summary>
        /// <param name="taskCLICommandTemplate">Command template string representing structure of the command without any task-specific information</param>
        /// <param name="tokenString">String placeholder in the template (e.g. "~s~")</param>
        /// <param name="targetString">String to be inserted into the task command string</param>
        /// <returns></returns>
        private static string UpdateTaskCLICommand(string taskCLICommandTemplate, string tokenString, string targetString)
        {
            return taskCLICommandTemplate.Replace(tokenString, targetString);
        }

        /// <summary>
        /// Updates the CLI commmand task template string to reflect the target scenario ID
        /// </summary>
        /// <param name="taskCLICommandTemplate"></param>
        /// <param name="scenarioID"></param>
        /// <returns></returns>
        private static string UpdateTaskCLICommand(string taskCLICommandTemplate, string scenarioID)
        {
            return taskCLICommandTemplate.Replace("~s~", scenarioID);
        }


        /// <summary>
        /// Given a full filepath, returns the "basename" of the file, which is typically constant across tasks/scenarios
        /// For example, filePath "C:\Users\Marko\model356.inp" will return "model356"
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private static string GetBaseFilenameFromFilePath(string filePath)
        {
            return Path.GetFileName(filePath).Split('.')[0];
        }

        /// <summary>
        /// Identifies the full directory paths to valid scenarios
        /// </summary>
        /// <param name="rootScenDir"></param>
        /// <param name="minScenarioID"></param>
        /// <param name="maxScenarioID"></param>
        /// <returns>List of target directory paths</returns>
        private static List<string> GetScenarioFilePaths(string rootScenDir, int minScenarioID, int maxScenarioID)
        {
            List<string> candidateDirs = Directory.GetDirectories(rootScenDir).ToList();
            List<string> selectedDirs = new List<string>();

            foreach(string cdir in candidateDirs)
            {
                int scenarioID = GetScenarioIDFromFilePath(cdir);

                if ((scenarioID >= minScenarioID) && (scenarioID <= maxScenarioID))
                {
                    selectedDirs.Add(cdir);
                }
            }
            return selectedDirs;
        }

        /// <summary>
        /// Gets the ScenarioID from a filepath (full directory or file path).
        /// Assumes that the deepest level that can be parsed to integer is the scenario ID.
        /// </summary>
        /// <param name="filePath">The full directory or file path</param>
        /// <returns></returns>
        private static int GetScenarioIDFromFilePath(string filePath)
        {
            string [] filePathPieces = filePath.Split(Path.DirectorySeparatorChar);
            int nPieces = filePathPieces.Length;
            int scenarioID;

            for(int i = (nPieces - 1); i >= 0; i--) //Loop backwards
            {
                bool bSuccess = Int32.TryParse(filePathPieces[i], out scenarioID);
                if (bSuccess)
                {
                    // Lowest dir level that is full integer is assumed to be scenario ID
                    // This also causes method exit so no need to break out of loop
                    return scenarioID; 
                }
            }
            string sMsg = string.Format("Could not extract scenario ID from path {0}", filePath);
            throw new Exception(sMsg);
        }


        /// <summary>
        /// Searches the target directories for input files that match the target pattern(s)
        /// </summary>
        /// <param name="targetDirList"></param>
        /// <param name="inputFileRegexList"></param>
        /// <returns>List of all filepaths in the target directories for which match was found</returns>
        private static List<string> GetInputFilePathList(List<string> targetDirList, List<string> inputFileRegexList)
        {
            HashSet<string> setInputFilePaths = new HashSet<string>(); //Use set to avoid unintentional dups
            Regex rx;

            foreach(string cdir in targetDirList)
            {
                foreach(string fp in Directory.GetFiles(cdir))
                {
                    foreach(string strrx in inputFileRegexList)
                    {
                        rx = new Regex(strrx);
                        if (rx.IsMatch(fp))
                        {
                            setInputFilePaths.Add(fp);
                        }
                    }
                }
            }
            return setInputFilePaths.ToList();
        }


        /// <summary>
        /// Extracts the scenario ID from a Task label
        /// Assumes task label format is TaskX where X is some integer
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        private static int GetScenarioIDFromTaskID(string taskId)
        {
            int scenarioID;
            bool bSuccess = Int32.TryParse(taskId.Replace("Task", ""), out scenarioID);
            if (!bSuccess)
            {
                string sMsg = string.Format("Could not extract scenarioID from task label {0}", taskId);
                throw new Exception(sMsg);
            }
            else
            {
                return scenarioID;
            }
        }

    }
}
