/// <summary>
/// Method to write relevant SupplyOpt results (flows at specified pump stations) to an Excel worksheet. 
/// Intended use is writing flow results to Excel after scenarios have been run, rather than on the fly. 
/// Faster because single file open/write/close operation, rather than performing those operations for each solution.
///
/// One sheet is added per SupplyOpt iteration, named like X_Y where X = scenario ID and Y = iteration #.
/// The Excel workbook file is written to the directory of the of the output database as specified in the
/// app_config.xml configuration file.  If the file does not already exist, it will be created. 
/// Existing sheets with duplicate names will be overwritten.
/// </summary>
public void WriteSolutionsToExcelFromDB(int nScenStart, int nScenEnd, bool bAllIter)
{

	// Get filepath and initialize of declare some local vars from Excel package
	Excel.Application xlApp = new Excel.Application();
	Excel.Workbook xlWorkBook;
	xlApp.Visible = false;
	string sFilePathExcel = GetSolutionOutputExcelFilePath();

	// Create file if it doesn't already exist
	if (!File.Exists(sFilePathExcel))
	{
		xlWorkBook = xlApp.Workbooks.Add();
		xlWorkBook.SaveAs(sFilePathExcel);
		xlWorkBook.Close();
	}
	else
	{
		string sMsg = string.Format("File {0} already exists. Appending or overwriting existing worksheets...", sFilePathExcel);
		log_handler.log(sMsg, log_handler.log_select.both, true, SimpleLog.Severity.Info);
	}

	// Initialize the workbook object
	xlWorkBook = xlApp.Workbooks.Open(sFilePathExcel);

	// Declare variables outside try/catch for use in error reporting
	int cnScenID = -1;
	int cnIter = -1;

	try
	{
		for (int nScenID = nScenStart; nScenID <= nScenEnd; nScenID++)
		{
			cnScenID = nScenID;
			List<int> lstIters = GetIterationList(nScenID, bAllIter);

			if ((lstIters == null) || (lstIters.Count == 0))
			{
				string sMsg1 = string.Format("Skipping scenario {0} because no results found.", nScenID);
				log_handler.log(sMsg1, log_handler.log_select.both, true);
				continue;  // Skip to next scenario
			}

			foreach (int nIter in lstIters)
			{
				cnIter = nIter;
				//Check if sheet already exists
				string sSheetName = "Run" + nScenID + "_" + nIter;
				foreach (Excel.Worksheet xl_sheet in xlWorkBook.Worksheets)
				{
					// If exists, delete
					if (xl_sheet.Name == sSheetName)
					{
						Excel.Worksheet xl_sheet_delete = xlWorkBook.Sheets[sSheetName];
						xlApp.DisplayAlerts = false;
						xl_sheet_delete.Delete();
						xlApp.DisplayAlerts = true;
						string sMsg2 = string.Format("Worksheet {0} already exists in SupplyOpt solutions output workbook- " +
							"deleting and replacing with current results.", sSheetName);
						log_handler.log(sMsg2, log_handler.log_select.both, true);
						break; // No need to keep searching
					}
				}

				// Get relevant flow results
				List<SpecificTimeSeries> lstSpecificTimeSeries = GetSupplyOptFlowResultsFromDB(nScenID, nIter);

				// Get relevant solution results-- if not provided as timeseries, extract from lstScenarioReults using name list
				if (lstSpecificTimeSeries == null || lstSpecificTimeSeries.Count == 0)
				{
					string sMsg3 = string.Format("Skipping results write for scenario {0} iteration {1} due to problem retrieving results (possibly infeasible)", nScenID, nIter);
					log_handler.log(sMsg3, log_handler.log_select.both, true, SimpleLog.Severity.Info);
					continue; // Move on to next iteration
				}

				//Add new sheet and name (important that comes after results check, so blank sheet not created)
				Excel.Worksheet xlWorkSheet;
				xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.Add(After: xlWorkBook.Worksheets[xlWorkBook.Worksheets.Count]);
				xlWorkSheet.Name = sSheetName;

				// Sort the list of timeseries by the label (alphabetically), to ensure columns appear in consistent
				// order across the scenarios/iterations
				lstSpecificTimeSeries = lstSpecificTimeSeries.OrderBy(x => x._sLabel).ToList();

				// Allcoate worksheet cell range for results
				int nCols = lstSpecificTimeSeries.Count + 1;  // Plus one for Timestamp column
				int nRows = lstSpecificTimeSeries[0]._lstTS.Count + 1; // Plus one for column headers

				// Fill the first column (timestamps) separately
				xlWorkSheet.Cells[1, 1] = "Timestamp";
				for (int i = 2; i <= nRows; i++)
				{
					xlWorkSheet.Cells[i, 1] = lstSpecificTimeSeries[0]._lstTS[i - 2]._dtDate;  // First SpecificTimeSeries (index 0).  Minus 2 to account for index base change and column header   
				}

				//Loop and fill Excel worksheet cell range.  NOTE: Excel module uses 1- base index
				// Start at column 2 because timestamp is in column 1 (again, 1-indexed)
				for (int i = 1; i <= nRows; i++)
				{
					for (int j = 2; j <= nCols; j++)
					{
						if (i == 1)
						{
							xlWorkSheet.Cells[i, j] = lstSpecificTimeSeries[j - 2]._sLabel;
						}
						else
						{
							xlWorkSheet.Cells[i, j] = lstSpecificTimeSeries[j - 2]._lstTS[i - 2]._dVal;
						}
					}
				}

				// Auto-fit the columns
				Excel.Range xlRange = xlWorkSheet.get_Range("A1", "A1"); // Autofit column with dates
				xlRange.Columns.AutoFit();
			}
		}

		Console.WriteLine();
		string sMsg = string.Format("Write to Excel operation complete.\n" +
			"File saved at: {0}", sFilePathExcel);
		Console.WriteLine(sMsg);
		Console.WriteLine();
	}

	catch (Exception ex)
	{
		// Save and close file
		string sMessage = string.Format("Something went wrong writing scenario {0}, iteration {1} to Excel: {2}", cnScenID, cnIter, ex.Message);
		log_handler.log(sMessage);
		throw new Exception(sMessage);
	}
	finally  // Always executed- whether exception or not
	{
		xlWorkBook.Save();
		xlWorkBook.Close();
	}
}